<?php

class ValueDiscount implements DiscountInterface
{
    use DiscountTrait;

    public function apply(Order $order): void
    {
        $orderPrice = $order->getTotalPrice();
        $products = $order->getProducts();

        foreach ($products as $product)
            $product->setPrice(round($product->getPrice() * (1 - $this->discount / $orderPrice)));
    }
}
