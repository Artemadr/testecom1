<?php

class PercentDiscount implements DiscountInterface
{
    use DiscountTrait;

    public function apply(Order $order): void
    {
        $products = $order->getProducts();

        foreach ($products as $product)
            $product->setPrice(round($product->getPrice() * (100 - $this->discount) / 100));
    }
}