<?php

interface DiscountInterface
{
    public function __construct(int|float $discount);


    public function apply(Order $order):void;
}