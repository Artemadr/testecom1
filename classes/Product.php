<?php

class Product
{
    public function __construct(protected string $name, protected float $price, protected int $quantity = 1)
    {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

}