<?php

trait DiscountTrait
{
    public function __construct(protected int|float $discount)
    {
    }
}