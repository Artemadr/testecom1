<?php

class Order
{
    /**
     * @var array<Product>
     */
    protected array $products = [];

    public function addProduct(Product $product): self
    {
        $this->products[] = $product;
        return $this;
    }

    public function setDiscount(DiscountInterface $discount): self
    {
        $discount->apply($this);
        return $this;
    }

    /**
     * @return array<Product> $products
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    public function getTotalPrice(): float
    {
        $orderCost = 0.0;
        foreach ($this->products as $product)
            $orderCost += $product->getPrice() * $product->getQuantity();

        return $orderCost;
    }

    public function showProducts(): self
    {
        foreach ($this->products as $product)
            echo "{$product->getName()}: {$product->getQuantity()} x {$product->getPrice()}\n";

        return $this;
    }
}