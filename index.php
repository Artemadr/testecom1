<?php
require "vendor/autoload.php";

(new Order())->addProduct(
    new Product('Кроссовки', 2000)
)
->addProduct(
    new Product('Шорты', 1000))
->addProduct(
    new Product('Футболка', 500)
)
->setDiscount(new PercentDiscount(30))
->showProducts();

echo "---------\n";

(new Order())->addProduct(
    new Product('Шорты', 1000, 3)
)
->setDiscount(new ValueDiscount(300))
->showProducts();
